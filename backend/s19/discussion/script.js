//alert("Hello, World!");

//display data via console
console.log("Hello World!")

// [SECTION] Variable
let my_variable = "Hola, Mundo!";
console.log(my_variable);


//Concatenating Strings
let country = "Philippines";
let province = "Metro Manila";

let full_address = province + ', ' + country;

console.log(full_address);

let headcount = 26;
let grade = 98.7;

console.log("The number of students is " + headcount + " and the average grade of all students is " + grade);

//let sum = headcount + grade;
//console.log(sum);

// Boolean
let isMarried = false;
let isGoodConduct = true;

console.log("He's married: " + isMarried);
console.log("She's a good person: " + isGoodConduct)

// Arrays
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);


// Objects

let person = {
	fullname: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["09992223313", "09448876691"],
	address: {
		houseNumber: "345",
		city: "England"
	}
}

console.log(typeof person);
console.log(typeof grades);

let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);


// [SECTION] Operators

// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Difference: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);

let assignment_number = 0;

assignment_number = assignment_number + 2;
console.log("Result of addition assignment operator: " + assignment_number)

assignment_number += 2;
console.log("Result of shorthand addition assignment operator: " + assignment_number);

//assignment_number ++;
//console.log(assignment_number);
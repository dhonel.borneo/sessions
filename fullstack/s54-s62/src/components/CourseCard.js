//import {Row, Col, Card, Button} from 'react-bootstrap';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from "react";

export default function CourseCard({course}){

	//Destructuring the contents of 'course'
	const {name, description, price} = course;

	// A state is just like a variable but with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible 
	const [count, setCount] = useState(0);
	const [seats, setSeat] = useState(30);

	//still working
	/*function enroll(){
		//The setCount function can use the previous value of the state and add/modify to it.
		setCount(prev_value => prev_value + 1);
		setSeat(prev_value => prev_value - 1);
		if(seats<=0){
			return alert("No more seats available");
		}
	}*/

	function enroll(){
		if(seats > 0){
			//The setCount function can use the previous value of the state and add/modify to it.
			setCount(prev_value => prev_value + 1);
			setSeat(prev_value => prev_value - 1);
		}else {
			return alert("No more seats available");
		}
	}


	//s55 Activity
	// 1. Create a 'seats' state that will have an initial value of 30
	// 2. Create a functionality wherein when you click the 'enroll' button, the 'seats' state will go down in value by 1 as well.
	// 3. Once the 'seats' state reaches 0 in value, show an alert that says 'No more seats avaialbe'.
	// 4. Push to gitlab once done.

	return(

	//<Row>
		//<Col >
				<Card>
				    <Card.Body>
				        <Card.Title>
				            {name}
				        </Card.Title>
				        <Card.Subtitle >Description:</Card.Subtitle>
				        <Card.Text>
				           {description}
				        </Card.Text>
				        <Card.Subtitle >Price</Card.Subtitle>
				        <Card.Text>
				           PHP {price}
				        </Card.Text>
				        <Card.Subtitle >Enrollees</Card.Subtitle>
				        <Card.Text>
				           {count}
				        </Card.Text>
				        <Card.Subtitle >Seats</Card.Subtitle>
				        <Card.Text>
				        	{seats}
				        </Card.Text>
				         <Button variant="primary" onClick={enroll}>Enroll</Button>
				    </Card.Body>
				</Card>
	//	</Col>
	//</Row>
	)
}

// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
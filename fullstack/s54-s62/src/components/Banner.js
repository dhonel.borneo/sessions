import {Button, Row, Col} from 'react-bootstrap';

export default function Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>DJ's Fisheries and Aquatic Resources</h1>
				<p>Fish tayo!</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	)
}